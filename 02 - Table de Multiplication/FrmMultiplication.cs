﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _02___Table_de_Multiplication
{
    public partial class FrmMultiplication : Form
    {
        public FrmMultiplication()
        {
            InitializeComponent();
        }

        private void FrmMultiplication_Load(object sender, EventArgs e)
        {
            // Todo 01 : Fixer la valeur minimale de numTable à 2


            // Todo 02 : Fixer la valeur maximale de numTable à 9

        }

        private void btnAfficher_Click(object sender, EventArgs e)
        {
            // Todo 03 : Afficher la table de multiplication choisie dans txtTable
            //           en utilisant une structure de contrôle itérative
            //
            //           Par exemple, pour la table de 2, 
            //           on doit obtenir le résultat suivant :
            //            1 x 2 =  2
            //            2 x 2 =  4
            //            3 x 2 =  6
            //            4 x 2 =  8
            //            5 x 2 = 10
            //            6 x 2 = 12
            //            7 x 2 = 14
            //            8 x 2 = 16
            //            9 x 2 = 18
            //           10 x 2 = 20
            //           
            //           Remarques : 
            //            - pour revenir à la ligne, utiliser Environnement.NewLine
            //            - pour concatener des chaines de charactères, on utilise l'opérateur +
            //              exemple : string str = "une chaine" + " et " + "une autre chaine";
            //              autre exemple : string str = "une chaine" + " et " + uneVariable;
            //                              => dans cet exemple C# va essayer de convertir la variable en chaine
            //                                 (tous les types de base peuvent être convertis en chaîne)



        }

        private void btnQuitter_Click(object sender, EventArgs e)
        {
            // Todo 04 : Quitter le programme


            // Bravo, vous avez terminé ce projet !
        }
    }
}
