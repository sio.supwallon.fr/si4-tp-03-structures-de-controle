﻿namespace _02___Table_de_Multiplication
{
    partial class FrmMultiplication
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblTable = new System.Windows.Forms.Label();
            this.numTable = new System.Windows.Forms.NumericUpDown();
            this.txtTable = new System.Windows.Forms.TextBox();
            this.btnQuitter = new System.Windows.Forms.Button();
            this.btnAfficher = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.numTable)).BeginInit();
            this.SuspendLayout();
            // 
            // lblTable
            // 
            this.lblTable.AutoSize = true;
            this.lblTable.Location = new System.Drawing.Point(12, 14);
            this.lblTable.Name = "lblTable";
            this.lblTable.Size = new System.Drawing.Size(87, 13);
            this.lblTable.TabIndex = 0;
            this.lblTable.Text = "Table à afficher :";
            // 
            // numTable
            // 
            this.numTable.Location = new System.Drawing.Point(105, 12);
            this.numTable.Name = "numTable";
            this.numTable.Size = new System.Drawing.Size(53, 20);
            this.numTable.TabIndex = 1;
            this.numTable.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.numTable.Value = new decimal(new int[] {
            2,
            0,
            0,
            0});
            // 
            // txtTable
            // 
            this.txtTable.Location = new System.Drawing.Point(12, 38);
            this.txtTable.Multiline = true;
            this.txtTable.Name = "txtTable";
            this.txtTable.Size = new System.Drawing.Size(146, 138);
            this.txtTable.TabIndex = 2;
            // 
            // btnQuitter
            // 
            this.btnQuitter.Location = new System.Drawing.Point(164, 153);
            this.btnQuitter.Name = "btnQuitter";
            this.btnQuitter.Size = new System.Drawing.Size(75, 23);
            this.btnQuitter.TabIndex = 4;
            this.btnQuitter.Text = "Quitter";
            this.btnQuitter.UseVisualStyleBackColor = true;
            this.btnQuitter.Click += new System.EventHandler(this.btnQuitter_Click);
            // 
            // btnAfficher
            // 
            this.btnAfficher.Location = new System.Drawing.Point(164, 38);
            this.btnAfficher.Name = "btnAfficher";
            this.btnAfficher.Size = new System.Drawing.Size(75, 23);
            this.btnAfficher.TabIndex = 3;
            this.btnAfficher.Text = "Afficher";
            this.btnAfficher.UseVisualStyleBackColor = true;
            this.btnAfficher.Click += new System.EventHandler(this.btnAfficher_Click);
            // 
            // FrmMultiplication
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(251, 188);
            this.Controls.Add(this.btnAfficher);
            this.Controls.Add(this.btnQuitter);
            this.Controls.Add(this.txtTable);
            this.Controls.Add(this.numTable);
            this.Controls.Add(this.lblTable);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmMultiplication";
            this.Text = "02 - Table de Multiplication";
            this.Load += new System.EventHandler(this.FrmMultiplication_Load);
            ((System.ComponentModel.ISupportInitialize)(this.numTable)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblTable;
        private System.Windows.Forms.NumericUpDown numTable;
        private System.Windows.Forms.TextBox txtTable;
        private System.Windows.Forms.Button btnQuitter;
        private System.Windows.Forms.Button btnAfficher;
    }
}

