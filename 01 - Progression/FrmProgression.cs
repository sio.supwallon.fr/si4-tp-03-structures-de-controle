﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _01___Progression
{
    public partial class FrmProgression : Form
    {
        public FrmProgression()
        {
            InitializeComponent();
        }

        private void btnLoadPicture_Click(object sender, EventArgs e)
        {
            // Todo 01 : Simuler le chargement d'une image à l'aide d'une itération
            //           A l'aide d'une structure de contrôle itérative donner au
            //           composant graphique progressBar1 les valeurs successives
            //           de 0 à 100 000


            // Todo 02 : Rendre la progressBar1 invisible (propriété Visible)


            // Todo 03 : Charger l'image keep-calm-and-love-c-sharp-150-175.png
            //           Charger l'image à l'aide de la méthode Image.FromFile
            //           Placer l'image dans la propriété Image de pictureBox1

        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            // Todo 04 : Ecrire l'instruction permettant de fermer le programme


            // Bravo, vous avez terminé ce projet !
        }
    }
}
