﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _03___Mosaic
{
    public partial class FrmMosaic : Form
    {
        public FrmMosaic()
        {
            InitializeComponent();
        }

        private void btnCharger_Click(object sender, EventArgs e)
        {
            // Todo 01 : Déclarer une variable index de type entier et l'initialiser à 0
            //           l'index correspondra au numéro de la pictureBox actuellement parcouru


            // Todo 02 : Parcourir les PictureBox présents dans le panel1 (propriété Controls)
            //           en utilisant la structure itérative foreach
            //           la variable de parcours sera de type PictureBox et s'appellera "courante"


            // Todo 03 : Déclarer les variables entières "ligne" et "colonne"
            //           au début de la structure itérative
            //           Ces variables correspondent à la position en ligne et colonne de la pictureBox dans le panel


            // Todo 04 : Calculer les valeurs de "ligne" et "colonne" à partir de l'index
            //           Ce calcul doit être écrit à l'intérieur de la structure itérative 
            //           pour prendre en compte le changement de valeur de l'index
            //
            //           indice 1 : 
            //            - les valeurs de index de  0 à  3 correspondent à la ligne 1 et les colonnes 1 à 4
            //            - les valeurs de index de  4 à  7 correspondent à la ligne 2 et les colonnes 1 à 4
            //            - les valeurs de index de  8 à 11 correspondent à la ligne 3 et les colonnes 1 à 4
            //            - les valeurs de index de 12 à 15 correspondent à la ligne 4 et les colonnes 1 à 4
            //            - etc..
            //
            //           indice 2 : 
            //            - pour calculer la ligne vous avez besoin d'une division et d'une addition
            //            - pour calculer la colonne vous avez besoin du reste de la division et d'une addition
            

            // Todo 05 : Charger l'image dans la PictureBox courante
            //           Utiliser la concaténation pour constuire le nom du fichier à charger
            //           Exemple pour ligne 1 et colonne 1 le fichier à charger est : row-1-col-1.jpg
            //           Charger l'image à l'aide de la méthode Image.FromFile
            //           Placer l'image dans la propriété Image de la PictureBox courante


            // Todo 06 : Incrémenter index à la fin de la structure itérative
            //           Placer l'incrémentation en dernière ligne à l'intérieur de la boucle

        }

        private void btnQuitter_Click(object sender, EventArgs e)
        {
            // Todo 07 : Fermer l'application


            // Bravo, vous avez terminé ce projet !
        }
    }
}
